class InterventionsController < ApplicationController
  def index
    params[:view] ||= 'map'
    @tags = Tag.with_collectives
    @interventions = find_interventions

    og_metas
  end

  private

  def find_interventions
    interventions = Seeker.new(params).find_interventions
      .upcoming
      .page(params[:page]).per(params[:per_page])
  end
end
