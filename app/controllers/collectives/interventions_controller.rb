class Collectives::InterventionsController < ApplicationController
  before_action :require_login, only: [:new, :edit, :update, :create, :destroy]
  before_action :find_intervention, only: [:show, :edit, :update, :destroy]
  before_action :find_collective, only: [:new, :index, :edit, :update, :create, :destroy]

  def new
    @tags = Tag.all
    authorize @collective, :create_intervention?
    @intervention = Intervention.new(collective: collective)
  end

  def index
    @tags = Tag.all
    @interventions = find_collective_interventions
  end

  def show
  end

  def edit
    @tags = Tag.all
    authorize @intervention, :edit?
  end

  def update
    authorize @intervention, :edit?
    @tags = Tag.all
    if @intervention.update(intervention_params)
      redirect_to [@collective, @intervention]
    else
      render 'edit'
    end
  end

  def create
    authorize @collective, :create_intervention?
    @tags = Tag.all
    @intervention = collective.interventions
      .create(intervention_params)
    if @intervention.persisted?
      redirect_to [@collective, @intervention]
    else
      render 'new'
    end
  end

  def destroy
    authorize @intervention, :edit?
    @intervention.destroy
    redirect_to @intervention.collective
  end

  private

  def intervention_params
    params.require(:intervention).permit(:name, :description,
                                         :place, :schedule, :event_link,
                                         :avatar, :cover,
                                         tag_ids: [])
  end

  def find_intervention
    @intervention = Intervention.friendly.find(params[:id])
  end

  def find_collective
    @collective = collective
  end

  def find_collective_interventions
    interventions = collective.interventions
    Seeker.new(params, interventions).find_interventions
      .page(params[:page]).per(params[:per_page])
  end

  def collective
    Collective.find(params[:collective_id])
  end
end
