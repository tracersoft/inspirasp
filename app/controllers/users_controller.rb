class UsersController < Clearance::UsersController
  private

  def user_params
    params.require(:user).permit(:email, :password)
  end

  def url_after_create
    if current_user.collective
      edit_collective_path(current_user.collective)
    else
      new_collective_path(collective: { name: params[:user].try(:[], :collective).try(:[], :name) })
    end
  end
end
