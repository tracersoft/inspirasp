class OmniauthController < ApplicationController
  def create
    user = find_or_create_user_by_auth_hash
    sign_in(user)
    redirect_to root_path
  end

  private

  def find_or_create_user_by_auth_hash
    auth_hash = request.env['omniauth.auth']
    User.find_or_create_from_auth_hash(auth_hash)
  end
end
