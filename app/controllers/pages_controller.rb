class PagesController < ApplicationController
  def about
    og_metas
  end

  def terms
    og_metas
  end
end
