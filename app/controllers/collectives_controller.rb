class CollectivesController < ApplicationController
  before_action :set_tags, only: [:new, :edit, :update, :create]
  before_action :set_collective, only: [:edit, :create, :update]
  before_action :require_login, only: [:new, :edit, :create, :update]

  def index
    @tags = Tag.all
    @collectives = find_collectives
    og_metas
  end

  def new
    if current_user.collective
      return redirect_to action: :edit, id: current_user.collective.id
    end
    collective_name = params[:collective] ? params[:collective][:name] : ''
    @collective = current_user.build_collective(name: collective_name)
  end

  def show
    @collective = Collective.friendly.find(params[:id])

    og_metas title:        @collective.name,
             description:  @collective.about,
             image:        request.base_url + Refile.attachment_url(@collective, :avatar)
  end

  def edit
    authorize @collective, :edit?
  end

  def create
    if @collective.save
      redirect_to @collective, notice: 'Cadastrado com sucesso!'
    else
      render action: "new"
    end
  end

  def update
    authorize @collective, :edit?

    if @collective.update(collective_params)
      redirect_to @collective, notice: 'Atualizado com sucesso.'
    else
      render action: "edit"
    end
  end

  private

  def set_tags
    @tags = Tag.all
  end

  def find_collectives
    Seeker.new(params).find_collectives
      .page(params[:page]).per(params[:per_page])
  end

  def set_collective
    @collective = current_user.collective ||
      current_user.build_collective(collective_params)
  end

  def collective_params
    params.require(:collective)
      .permit(:name, :address , :email, :phone, :about,
              :facebook_link, :responsible, :avatar, :cover,
              :website,
              tag_ids: [])
  end
end
