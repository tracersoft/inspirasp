class HomeController < ApplicationController
  def testimonials
    [
      {
        author: "Movimento Boa Praça",
        phrase: "O que você acha de comemorar o seu próximo aniversário na pracinha do lado da sua casa?",
        image: "boa-praca",
        url: "/boa-praca"
      },
      {
        author: "Baixo Centro",
        phrase: '“As ruas são para dançar”',
        url: "/baixo-centro",
        image: "baixo-centro"
      },
      {
        author: "A Batata Precisa de Você",
        phrase: "Receita para transformar o Deserto em um Oásis cultural",
        url: "/batata-precisa-de-voce",
        image: "batata-precisa-de-voce"
      }
    ]
  end

  def index
    if signed_in?
      unless current_user.collective
        return redirect_to new_collective_path
      end
      @collectives = Collective.limit(3).order("RANDOM()")
    else
      @collectives = Collective.limit(2).order("RANDOM()")
    end
    @testimonial = testimonials[rand(0..2)]
    @user = User.new

    og_metas
  end

  def search
    @collectives = PgSearch.multisearch(params[:query])
      .where(searchable_type: 'Collective').map(&:searchable)
    @interventions = PgSearch.multisearch(params[:query])
      .where(searchable_type: 'Intervention').map(&:searchable)
    render action: 'index'
  end
end
