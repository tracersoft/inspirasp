class ApplicationController < ActionController::Base
  include Clearance::Controller
  include Pundit
  include OgHelper
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."
    path = if request.referrer && !request.referrer =~ /users\//
      request.referrer
    else
      root_path
    end
    redirect_to path
  end
end
