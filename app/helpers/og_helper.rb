module OgHelper
  include ActionView::Helpers::TextHelper

  def og_metas(og_metas = {})
    name_default        = "inSPira"
    id_default          = ENV['FACEBOOK_KEY']
    title_default       = "Queremos inSPirar uma nova atitude na cidade!"
    description_default = "Fomos inspirados a ter uma nova atitude com a cidade, agora queremos conectar esse movimento!"
    image_default       = request.base_url + ActionController::Base.helpers.image_url('logo.png')

    set_meta_tags fb: {
        app_id:       id_default,
      },
      og: {
        site_name:    name_default,
        title:        og_metas.blank? ? title_default : og_metas.fetch(:title),
        description:  og_metas.blank? ? description_default : truncate(og_metas.fetch(:description), length: 240),
        type:         "article",
        locale:       I18n.locale,
        url:          request.original_url,
        image:        og_metas.blank? ? image_default : og_metas.fetch(:image),
      },
      twitter:{
        title:        name_default,
        description:  og_metas.blank? ? description_default : truncate(og_metas.fetch(:description), length: 240),
        image:        og_metas.blank? ? image_default : og_metas.fetch(:image),
        card:         "post",
      }
  end
end
