module ApplicationHelper
  @@default_tags = Tag.all

  def current_collective
    current_user.collective
  end

  def present(model, presenter_class=nil)
    klass = presenter_class || "#{model.class}Presenter".constantize
    presenter = klass.new(model, self)
    yield(presenter) if block_given?
  end

  def tags
    @tags || @@default_tags
  end

  def search_params
    params.slice(:query, :tag, :view)
  end

  def rand_cover
    image_path("covers/cover-#{rand(1..3)}.jpg")
  end
end
