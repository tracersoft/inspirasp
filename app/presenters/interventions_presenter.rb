class InterventionsPresenter
  include Refile::AttachmentHelper
  include Rails.application.routes.url_helpers

  def self.present(collection)
    new(collection).present
  end

  def initialize(collection)
    @collection = collection
  end

  def present
    Array(@collection).map do |item|
      serialize item
    end
  end

  private

  def serialize(item)
    item.as_json(only: [:name, :place])
      .merge!(additional_params_for(item))
  end

  def additional_params_for(item)
    {
      avatar_url: attachment_url(item, :avatar, :fill, 300, 200),
      href: collective_intervention_path(item.collective, item),
      collective_url: collective_path(item.collective),
      collective_name: item.collective.name,
      collective_avatar: attachment_url(item.collective, :avatar, :fill, 100, 100),
      tags: tags_for(item),
      schedule: I18n.l(item.schedule, format: :short)
    }
  end

  def tags_for(item)
    item.tags.map do |tag|
      {
        name: tag.name,
        param: tag.name.parameterize
      }
    end
  end
end
