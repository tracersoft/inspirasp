class CollectiveContract < Reform::Contract
  property :name, validates: { presence: true }
  property :email, validates: { presence: true }
  property :avatar, validates: { presence: true }
  property :about, validates: { presence: true }
  property :email, validates: { presence: true }
  property :responsible, validates: { presence: true }
end
