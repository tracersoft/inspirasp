class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    default_permission
  end

  def new?
    create?
  end

  def update?
    default_permission
  end

  def edit?
    update?
  end

  def destroy?
    default_permission
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope
    end
  end

  protected

  def default_permission
    record.respond_to?(:user) && record.user == user
  end
end
