class CollectivePolicy < ApplicationPolicy
  def create_intervention?
    record.user_id == user.id
  end
end
