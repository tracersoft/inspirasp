class InterventionPolicy < ApplicationPolicy
  def edit?
    user == record.user
  end
end
