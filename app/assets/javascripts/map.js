$(function() {
  function Map($container) {
    this.$container = $container;
    this.map = L.map($container[0], { minZoom: 2, maxZoom: 16 })
      .setView([-23.58, -46.61], 11);
    L.tileLayer('https://{s}.tiles.mapbox.com/v3/mpivaa.kgcn043g/{z}/{x}/{y}.png',
      { attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>' })
      .addTo(this.map);
    this.map.scrollWheelZoom.disable();
  }

  Map.prototype.load = function() {
    var locales = this.$container.data('locales');
    var map = this.map;
    this.geocodeLocales(locales);
    $.getJSON('/sp.geojson', function(data) {
      L.geoJson(data, {
        style: {
          opacity: 0.6,
          color: "#c0272d",
          fill: false,
          dashArray: "5, 5",
          weight: 2
        }
      }).addTo(map);
    });
  }

  Map.prototype.addMarker = function(locale, position) {
    var popup = L.popup({ minHeight: 500})
      .setContent(HandlebarsTemplates['map/popups/intervention'](locale));
    L.marker([position.Y, position.X])
      .addTo(this.map)
      .bindPopup(popup);
  }

  Map.prototype.geocodeLocales = function(locales) {
    var geocodeProvider = new L.GeoSearch.Provider.Esri();
    for(var i in locales) {
      var locale = locales[i];
      var address = locale.place + ', Brasil';
      var url = geocodeProvider.GetServiceUrl(address);

      (function(locale, map) {
        $.getJSON(url, function(data) {
          var results = geocodeProvider.ParseJSON(data)
          if(results.length) {
            map.addMarker(locale, results[0])
          }
        });
      })(locale, this)
    }
  }

  if($('#map').length) {
    var map = new Map($('#map'));
    map.load();
  }
})
