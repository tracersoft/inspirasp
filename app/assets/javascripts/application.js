// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
//= require jquery
//= require jquery_ujs
//= require leaflet
//= require vendors
//= require handlebars.runtime
//= require_tree ./templates
//= require_tree .
