(function () {
  $(document).ready(function(){
    $('[data-cover]').each(function(){
      var $this = $(this),
          $image = $this.data('cover');

      $this.css({
        backgroundImage: 'url("' + $image + '")'
      });
    });
  });
})($);
