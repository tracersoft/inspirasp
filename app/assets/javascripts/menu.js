(function () {
  $(document).ready(function(){
    $('[data-menu-mobile]').bind('click', function(e){
      var $this   = $(this),
          $menu   = $('html'),
          $class  = 'menu-open';

      $menu.toggleClass($class);
    });
  });
})($);
