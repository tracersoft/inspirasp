(function () {
  $(document).ready(function(){
    $('[data-show-element]').click(function(){
      var $this = $(this),
          $images = $this.data('show-element');

      $this.hide(200);
      $($images).show(200);
    });
  });
})($);
