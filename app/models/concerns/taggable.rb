module Taggable
  def self.included(base)
    base.class_eval do
      scope :by_tag_id, -> (*tag_ids){
        if tag_ids.include?('any')
          all
        else
          joins(:taggable_tags)
            .where('taggable_tags.tag_id IN (?)', tag_ids)
            .group("#{table_name}.id", :name)
        end
      }
    end
  end

  def tag_names
    tags.pluck(:name).join(', ')
  end
end
