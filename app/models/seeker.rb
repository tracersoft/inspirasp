class Seeker
  def initialize(params = {}, scope = nil)
    @tag = params.fetch(:tag, 'any')
    @query = params[:query]
    @scope = scope
  end

  def find_all
    filter_by_tag(PgSearch.multisearch(@query))
  end

  def find_collectives
    seek_and_filter(scope: @scope || default_collectives_scope)
  end

  def find_interventions
    seek_and_filter(scope: @scope || default_interventions_scope)
  end

  private

  def seek_and_filter(scope: default_collectives_scope)
    resources = scope
    resources = filter_by_tag(resources)
    resources = filter_by_query(resources)
    resources
  end

  def filter_by_tag(resource)
    return resource unless @tag.present?
    resource.by_tag_id(@tag)
  end

  def filter_by_query(resource)
    return resource unless @query.present?
    resource.search(@query).group("pg_search.rank, #{resource.table_name}.id")
  end

  def default_collectives_scope
    Collective.all.order(created_at: :desc)
  end

  def default_interventions_scope
    Intervention.all.order(created_at: :desc)
  end
end
