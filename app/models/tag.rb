class Tag < ActiveRecord::Base
  has_many :taggable_tags
  has_many :collectives, through: :taggable_tags, source: :taggable, source_type: 'Collective'
  has_many :interventions, through: :taggable_tags, source: :taggable, source_type: 'Intervention'

  scope :with_collectives, (lambda do
    joins(:taggable_tags)
      .where("taggable_tags.taggable_type = 'Collective'")
      .group('tags.id', 'tags.name')
  end)
end
