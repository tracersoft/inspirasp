class Collective < ActiveRecord::Base
  include Taggable
  include PgSearch
  extend FriendlyId

  belongs_to :user
  has_many :interventions, dependent: :delete_all
  has_many :taggable_tags, as: :taggable
  has_many :tags, through: :taggable_tags

  validates_presence_of :name
  validates_presence_of :avatar
  validates_presence_of :cover
  validates_presence_of :about
  validates_presence_of :email
  validates :website, :url => { allow_nil: true, allow_blank: true }
  validates_email_format_of :email

  attachment :avatar
  attachment :cover

  multisearchable against: [:name, :about]
  pg_search_scope :search,
    against: [:name, :about],
    using: [:tsearch, :trigram, :dmetaphone],
    ignoring: :accents

  paginates_per 12

  friendly_id :name, use: [:slugged, :finders]

  def to_param
    self.slug
  end
end
