class User < ActiveRecord::Base
  include Clearance::User
  has_one :collective

  validates :terms, acceptance: { accept: true }

  def self.find_or_create_from_auth_hash(auth_hash)
    where(provider: auth_hash[:provider], uid: auth_hash[:uid]).first_or_create do |user|
      user.uid = auth_hash[:uid]
      user.provider = auth_hash[:provider]
      user.email = auth_hash[:info][:email]
      user.password = SecureRandom.hex(16)
    end
  end
end
