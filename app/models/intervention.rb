class Intervention < ActiveRecord::Base
  include PgSearch
  include Taggable
  extend FriendlyId

  belongs_to :collective
  has_one :user, through: :collective
  has_many :taggable_tags, as: :taggable
  has_many :tags, through: :taggable_tags

  validates :name, :description, :schedule, :avatar, :cover, :place, presence: true

  validates :event_link, :url => { allow_nil: true, allow_blank: true }
  validates_presence_of :collective

  attachment :avatar
  attachment :cover

  friendly_id :name, use: [:slugged, :finders]

  multisearchable against: [:name, :description]
  pg_search_scope :search,
    against: [:name, :description],
    using: [:tsearch, :trigram, :dmetaphone],
    ignoring: :accents

  paginates_per 12

  scope :upcoming, -> { where('schedule > ?', Time.current) }

  def to_param
    self.slug
  end
end
