require 'rails_helper'

describe CollectivePresenter do
  include Capybara::RSpecMatchers

  let(:collective) { create(:collective) }
  let(:presenter) { CollectivePresenter.new(collective) }
end
