require 'rails_helper'

feature 'Signing up' do
  background do
    visit sign_up_path
  end

  scenario 'creates a collective' do
    within '#new_user' do
      fill_in 'user[email]', with: 'user@example.com'
      fill_in 'user[password]', with: 'jabuticabeira-amarelada'
      fill_in 'user[collective][name]', with: 'Praça Jabuticabeira'
      click_button 'Registrar-se'
    end
    expect(page.current_path).to eq(new_collective_path)
  end
end
