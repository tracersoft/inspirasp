require 'rails_helper'

feature 'Searching collectives' do
  given!(:desired_collective) { create(:collective, name: 'Praça Werner & Gauss') }
  given!(:undesired_collective) { create(:collective, name: 'Johnson & Johnson') }

  background do
    visit collectives_path
  end

  scenario 'searching a specific collective' do
    within '#search' do
      fill_in 'query', with: 'Werner Gauss'
      select 'Todos interesses', from: 'tag'
      click_on 'Buscar'
    end
    expect(page).to have_text(desired_collective.name)
    expect(page).not_to have_text(undesired_collective.name)
  end
end
