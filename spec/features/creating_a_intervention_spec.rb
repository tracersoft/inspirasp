require 'rails_helper'

feature 'Creating a collective' do
  given!(:tag) { create(:tag, name: 'Urbanismo') }
  given!(:collective) { create(:collective, user: user) }
  given(:user) { create(:user) }

  background do
    visit new_collective_intervention_path(collective, as: user)
  end

  scenario 'with tags' do
    within '#new_intervention' do
      fill_in 'intervention[name]', with: 'Praça Exemplo'
      fill_in 'intervention[place]', with: 'Local da praça'
      fill_in 'intervention[description]', with: 'Lorem ipsum dolor sit amet'
      fill_in 'intervention[event_link]', with: 'facebook.com/coletivoExemplo'
      find_by_id("intervention_tag_ids_#{tag.id}").set(true)
      click_button 'Criar Evento'
    end
    expect(page).to have_text(tag.name)
  end
end
