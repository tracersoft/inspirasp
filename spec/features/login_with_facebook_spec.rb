require 'rails_helper'

feature 'Signing up with facebook' do
  background do
    OmniAuth.config.add_mock(:facebook, { uid: '12345', info: { email: 'ba@ta.ta' } })
    visit sign_in_path
  end

  scenario 'creates an account' do
    pending '¯\_(ツ)_/¯'
    within("#clearance") do
      click_on('login_with_facebook')

      expect(page).to have_content('ba@ta.ta')
    end
  end
end
