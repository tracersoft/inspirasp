require 'rails_helper'

feature 'Creating a collective' do
  given!(:tag) { create(:tag, name: 'Urbanismo') }
  given(:user) { create(:user) }

  background do
    visit new_collective_path(as: user)
  end

  scenario 'with tags' do
    within '#new_collective' do
      fill_in 'collective[name]', with: 'Praça Exemplo'
      fill_in 'collective[email]', with: 'praca@exemplo.org'
      fill_in 'collective[address]', with: 'Local da praça'
      fill_in 'collective[about]', with: 'Lorem ipsum dolor sit amet'
      fill_in 'collective[facebook_link]', with: 'facebook.com/coletivoExemplo'
      find_by_id("collective_tag_ids_#{tag.id}").set(true)
      click_button 'Criar Praça'
    end

    expect(page).to have_text(tag.name)
  end
end
