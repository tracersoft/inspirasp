require 'rails_helper'

feature 'Searching interventions' do
  given(:collective) { create(:collective) }
  given!(:desired_intervention) { create(:intervention, name: 'Praça Werner & Gauss', collective: collective) }
  given!(:undesired_intervention) { create(:intervention, name: 'Johnson & Johnson', collective: collective) }

  background do
    visit collective_interventions_path(collective)
  end

  scenario 'searching a specific collective' do
    within '#search' do
      fill_in 'query', with: 'Werner Gauss'
      select 'Todos interesses', from: 'tag'
      click_on 'Buscar'
    end
    expect(page).to have_text(desired_intervention.name)
    expect(page).not_to have_text(undesired_intervention.name)
  end
end
