require 'rails_helper'

describe User do
  context 'when the user has not accepeted the terms' do
    let(:user) { build(:user, terms: false) }

    it 'is not valid' do
      expect(user).not_to be_valid
    end
  end
end
