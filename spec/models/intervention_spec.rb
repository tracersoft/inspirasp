require 'rails_helper'

RSpec.describe Intervention, type: :model do
  it { is_expected.to belong_to(:collective) }
  it { is_expected.to have_one(:user).through(:collective) }
  it { is_expected.to have_many(:tags) }
end
