FactoryGirl.define do
  factory :collective do
    name { FFaker::Company.name }
    address { FFaker::AddressBR.street }
    about { FFaker::Company.catch_phrase }
    facebook_link "facebook.com/collective"
    avatar_id { 'ana8sd7hfisdf' }
    cover_id { 'ana8sd7hfisdf' }
    email

    trait :without_facebook do
      facebook_link ''
    end
  end
end
