FactoryGirl.define do
  factory :intervention do
    name { FFaker::Conference.name }
    description { FFaker::BaconIpsum.words(30).to_sentence }
    schedule { 2.days.from_now }
    collective
    place { "place" }
    avatar_id { 'ana8sd7hfisdf' }
    cover_id { 'ana8sd7hfisdf' }
  end
end
