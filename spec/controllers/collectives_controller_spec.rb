require 'rails_helper'

RSpec.describe CollectivesController, type: :controller do
  let(:user) { create(:user) }
  let(:file) { fixture_file_upload('files/image.png')}

  before do
    sign_in_as(user)
  end

  let(:valid_attributes) { {name: "collective name" , email: "collective@email.com",
   address: "Street XXX, 192", phone: "123 1234 1234", about: "collective about",
   facebook_link: "facebook.com/collective" , responsible: "collective responsible",
   user_id: user.id, avatar: file, cover: file } }

  let(:invalid_attributes) { {address: "Street XXX, 192",
    phone: "123 1234 1234", about: "collective about", facebook_link: "facebook.com/collective" , responsible: "collective responsible" } }

  let(:valid_session) { {} }

  describe 'GET index' do
    let!(:collectives) { create_list(:collective, 3).sort_by(&:created_at) }

    it 'assigns collectives' do
      get :index, { locale: 'pt-BR' }
      expect(assigns(:collectives)).to match_array(collectives)
    end

    it 'comes paginated' do
      get :index, { locale: 'pt-BR', page: 1, per_page: 2 }
      expect(assigns(:collectives)).to match_array(collectives.last(2))
    end
  end

  describe "GET #show" do
    it "assigns the requested collective as @collective" do
      collective = Collective.create! valid_attributes
      get :show, {locale: 'pt-BR', :id => collective.to_param}, valid_session
      expect(assigns(:collective)).to eq(collective)
    end
  end

  describe "GET #new" do
    it "assigns a new collective as @collective" do
      get :new, {locale: 'pt-BR'}, valid_session
      expect(assigns(:collective)).to be_a_new(Collective)
    end
  end

  describe "GET #edit" do
    it "assigns the requested collective as @collective" do
      collective = Collective.create! valid_attributes
      get :edit, {locale: 'pt-BR', :id => collective.to_param}, valid_session
      expect(assigns(:collective)).to eq(collective)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Collective" do
        expect {
          post :create, {locale: 'pt-BR', :collective => valid_attributes}, valid_session
        }.to change(Collective, :count).by(1)
      end

      it "assigns a newly created collective as @collective" do
        post :create, {locale: 'pt-BR', :collective => valid_attributes}, valid_session
        expect(assigns(:collective)).to be_a(Collective)
        expect(assigns(:collective)).to be_persisted
      end

      it "redirects to the created collective" do
        post :create, {locale: 'pt-BR', :collective => valid_attributes}, valid_session
        expect(response).to redirect_to(Collective.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved collective as @collective" do
        post :create, {locale: 'pt-BR', :collective => invalid_attributes}, valid_session
        expect(assigns(:collective)).to be_a_new(Collective)
      end

      it "re-renders the 'new' template" do
        post :create, {locale: 'pt-BR', :collective => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { {name: "valid" , email: "collective@email.com",
   address: "Street XXX, 192", phone: "123 1234 1234", about: "collective about",
   facebook_link: "facebook.com/collective" , responsible: "collective responsible" }  }

      it "assigns the requested collective as @collective" do
        collective = Collective.create! valid_attributes
        put :update, {locale: 'pt-BR', :id => collective.to_param, :collective => valid_attributes}, valid_session
        expect(assigns(:collective)).to eq(collective)
      end

      it "redirects to the collective" do
        collective = Collective.create! valid_attributes
        put :update, {locale: 'pt-BR', :id => collective.to_param, :collective => valid_attributes}, valid_session
        expect(response).to redirect_to(collective)
      end
    end

    context "with invalid params" do
      it "assigns the collective as @collective" do
        collective = Collective.create! valid_attributes
        put :update, {locale: 'pt-BR', :id => collective.to_param, :collective => invalid_attributes}, valid_session
        expect(assigns(:collective)).to eq(collective)
      end
    end
  end
end
