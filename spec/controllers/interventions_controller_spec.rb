require 'rails_helper'

RSpec.describe InterventionsController do
  describe 'GET index' do
    let!(:interventions) { create_list(:intervention, 3) }

    it 'returns paginated tags' do
      get :index, locale: 'pt-BR', page: 1, per_page: 2
      expect(assigns(:interventions)).to match_array(interventions.last(2))
    end

    context 'when there are interventions which have passed' do
      let!(:old_intervention) { create(:intervention, schedule: 1.hour.ago) }

      it 'contains only upcoming interventions' do
        get :index, locale: 'pt-BR'
        expect(assigns(:interventions)).not_to include(old_intervention)
      end
    end
  end
end
