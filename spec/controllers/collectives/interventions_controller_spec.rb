require 'rails_helper'

RSpec.describe Collectives::InterventionsController, type: :controller do
  let(:user) { create(:user) }
  let(:collective) { create(:collective, user: user) }
  let!(:owned_intervention) { create(:intervention, collective: collective) }
  let!(:random_intervention) { create(:intervention) }

  describe 'GET index' do
    it 'shows only interventions associated with the colelctive' do
      get :index, locale: 'en', collective_id: collective.id
      expect(assigns(:interventions)).to include(owned_intervention)
      expect(assigns(:interventions)).not_to include(random_intervention)
    end

    it 'comes paginated' do
      get :index, { collective_id: collective.id, locale: 'pt-BR', page: 1, per_page: 1 }
      expect(assigns(:interventions).size).to eq(1)
    end
  end

  describe 'GET show' do
    it 'shows the intervention' do
      get :show, locale: 'en', collective_id: collective.id, id: owned_intervention.id
      expect(assigns(:intervention)).to eq(owned_intervention)
    end
  end

  describe 'GET edit' do
    context 'when user is not authenticated' do
      it 'redirects to login' do
        get :edit, locale: 'en', collective_id: collective.id, id: random_intervention.id
        expect(response).to redirect_to(sign_in_path)
      end
    end
  end

  describe 'POST create' do
    context 'when user is logged in' do
      before do
        sign_in_as(user)
      end

      let(:file) { fixture_file_upload('files/image.png') }

      let(:params) do
        {
          name: 'Encontro da Praça ProjetAção',
          description: 'Dolorem Ipsum dolor sit amet',
          place: 'Palácio do Planalto',
          event_link: 'http://facebook.com/events/2345678/ta-na-hora',
          schedule: '25/02/2058',
          avatar: file,
          cover: file
        }
      end

      it 'creates a intervention' do
        expect { post :create, locale: 'en', collective_id: collective.id, intervention: params }
          .to change(Intervention, :count).by(1)
      end

      context 'when the user does not own the collective' do
        before do
          sign_in_as(user)
        end

        it 'does not permit the creation' do
          expect { post :create, locale: 'en', collective_id: random_intervention.collective_id,
                   intervention: params }
            .not_to change(Intervention, :count)
        end
      end
    end
  end

  describe 'PUT update' do
    context 'when user is logged in' do
      before do
        sign_in_as(user)
      end

      let(:params) do
        {
          name: 'Encontro da Praça ProjetAção',
          place: 'Palácio do Planalto',
          event_link: 'http://facebook.com/events/2345678/ta-na-hora',
          schedule: '25/02/2058',
        }
      end

      it 'creates a intervention' do
        put :update, locale: 'en', collective_id: collective.id,
          intervention: params, id: owned_intervention.id
        owned_intervention.reload
        expect(owned_intervention.name).to eq('Encontro da Praça ProjetAção')
      end
    end
  end
end
