require 'rails_helper'

describe HomeController do
  describe 'GET search' do
    let(:collective) { create(:collective, name: 'Werner') }
    let!(:unwanted_collective) { create(:collective, name: 'Lambda') }
    let!(:desired_intervention) { create(:intervention, name: 'Praça Werner & Gauss', collective: collective) }
    let!(:undesired_intervention) { create(:intervention, name: 'Johnson & Johnson', collective: collective) }

    before do
      get :search, query: 'Werrner', tag: 'any'
    end

    it 'returns only desired interventions' do
      expect(assigns(:interventions)).to include(desired_intervention)
      expect(assigns(:interventions)).not_to include(undesired_intervention)
    end

    it 'returns desired collectives' do
      expect(assigns(:collectives)).to include(collective)
      expect(assigns(:collectives)).not_to include(unwanted_collective)
    end
  end
end
