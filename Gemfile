source 'https://rubygems.org'

ruby '2.2.0'
gem 'rails', '4.2.1'

gem 'burgundy'
gem 'clearance'
gem 'friendly_id'
gem 'jbuilder', '~> 2.0'
gem 'jquery-rails'
gem 'kaminari'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'pg'
gem 'pg_search', '~> 1.0.3'
gem 'puma', '~> 2.11.3'
gem 'pundit'
gem 'reform'
gem 'refile', require: 'refile/rails'
gem 'refile-mini_magick'
gem 'route_translator'
gem 'sass-rails', '~> 5.0'
gem 'simple_form'
gem 'uglifier', '>= 1.3.0'
gem 'flutie'
gem 'title'
gem 'validates_email_format_of'
gem "validate_url"
gem 'meta-tags'

# Front-end
gem 'bourbon', '~> 4.2.0'
gem 'neat', '~> 1.7.0'
gem 'normalize-rails', '~> 3.0.0'
gem 'leaflet-rails'
gem 'handlebars_assets'

group :production do
  gem 'aws-sdk'
  gem 'newrelic_rpm'
  gem 'rails_12factor'
  gem 'refile-s3'
  gem 'unicorn'
end

group :development, :test do
  gem 'pry-rails'
  gem 'web-console', '~> 2.0'
  gem 'spring'
  gem 'dotenv-rails'
  gem 'ffaker'
end

group :test do
  gem 'rspec-rails'
  gem 'capybara'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers'
  gem 'launchy'
end
