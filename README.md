## InspiraSP

### Setting up

```bash
bin/setup
```

### Deploying

Set all necessary environment variables (available variables in `.env.sample`).
