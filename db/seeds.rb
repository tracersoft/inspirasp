# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
[
  'Parques, praças e ruas',
  'Oficinas e Educação',
  'Arte e cultura',
  'Meio ambiente',
  'Ciência e Tecnologia',
  'Gastronomia',
  'Arquitetura e Urbanismo',
  'Idosos',
  'Crianças',
  'Jogos, Esporte e Lazer',
  'Eventos',
  'Transporte e Mobilidade'
].each do |tag_name|
  Tag.find_or_create_by!(name: tag_name)
end

