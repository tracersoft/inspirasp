class MakeCollectiveEmailOptional < ActiveRecord::Migration
  def change
    reversible do |dir|
      dir.up do
        change_column :collectives, :email, :string, null: true
      end
      dir.down do
        change_column :collectives, :email, :string, null: false
      end
    end
  end
end
