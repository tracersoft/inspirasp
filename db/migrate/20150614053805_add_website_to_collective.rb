class AddWebsiteToCollective < ActiveRecord::Migration
  def change
    add_column :collectives, :website, :string
  end
end
