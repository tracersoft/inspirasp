class AddUserToCollective < ActiveRecord::Migration
  def change
    add_reference :collectives, :user, index: true, foreign_key: true
  end
end
