class CreateInterventions < ActiveRecord::Migration
  def change
    create_table :interventions do |t|
      t.references :collective, index: true, foreign_key: true
      t.string :name, null: false, default: ''
      t.text :description
      t.string :place
      t.datetime :schedule
      t.string :event_link

      t.timestamps null: false
    end
  end
end
