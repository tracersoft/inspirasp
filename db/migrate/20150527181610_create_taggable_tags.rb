class CreateTaggableTags < ActiveRecord::Migration
  def change
    create_table :taggable_tags do |t|
      t.references :tag, index: true, foreign_key: true
      t.references :taggable, index: true, polymorphic: true

      t.timestamps null: false
    end
  end
end
