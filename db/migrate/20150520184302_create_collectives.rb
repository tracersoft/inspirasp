class CreateCollectives < ActiveRecord::Migration
  def change
    create_table :collectives do |t|
      t.string :name, null: false
      t.string :address
      t.string :email, null: false
      t.string :phone
      t.text :about
      t.string :facebook_link
      t.string :responsible

      t.timestamps null: false
    end

    add_index :collectives, :name
  end
end
