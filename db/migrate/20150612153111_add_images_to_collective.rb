class AddImagesToCollective < ActiveRecord::Migration
  def change
    add_column :collectives, :avatar_id, :string
    add_column :collectives, :cover_id, :string
  end
end
