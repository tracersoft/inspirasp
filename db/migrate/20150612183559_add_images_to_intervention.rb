class AddImagesToIntervention < ActiveRecord::Migration
  def change
    add_column :interventions, :avatar_id, :string
    add_column :interventions, :cover_id, :string
  end
end
