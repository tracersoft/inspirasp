PgSearch.multisearch_options = {
  :using => [:tsearch, :trigram, :dmetaphone],
  :ignoring => :accents
}
