Rails.application.routes.draw do
  get '/sobre' => 'pages#about'
  get '/termos-de-uso' => 'pages#terms', as: 'termos'
  get '/batata-precisa-de-voce' => 'pages#batata-precisa-de-voce'
  get '/baixo-centro' => 'pages#baixo-centro'
  get '/boa-praca' => 'pages#boa-praca'

  get '/search', to: 'home#search'
  get '/auth/:provider/callback' => 'omniauth#create'

  resources :users,
    :controller => 'users',
    :only => [:new, :create, :destroy]

  root to: 'home#index'

  localized do
    resources :interventions, only: [:index]
    resources :collectives, only: [:create, :index, :show, :edit, :update, :new] do
      resources :interventions, module: 'collectives'
    end
  end
end
